//
//  main.m
//  iOSSampleApp
//
//  Created by So TANAKA on 2014/04/22.
//  Copyright (c) 2014年 So TANAKA. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
